import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';


import { FeedPage } from '../pages/feed/feed';

import { Camera } from '@ionic-native/camera'

import firebase from 'firebase'


var firebaseConfig = {
  apiKey: "AIzaSyDi6xq2ctt43EJJ4m3Z99Cj6ldLDfJROiQ",
  authDomain: "feedly-c782f.firebaseapp.com",
  databaseURL: "https://feedly-c782f.firebaseio.com",
  projectId: "feedly-c782f",
  storageBucket: "feedly-c782f.appspot.com",
  messagingSenderId: "1076834636983"
};
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({
  timestampsInSnapshots: true
})


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    SignupPage,
    FeedPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    SignupPage,
    FeedPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
